package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"os"

	"gitlab.com/fredsar/htmlparser/internal/usecase"
)

func main() {
	file, err := initialize()
	if err != nil {
		log.Panic(err)
	}
	links, err := usecase.GetLinksFromHTML(file)
	if err != nil {
		log.Panic(err)
	}

	fmt.Printf("%+v\n", links)
}

func parseFlags() string {
	htmlFile := flag.String("file", "ex1.html", "The HTML file to parse")
	flag.Parse()

	return *htmlFile
}

func initialize() (io.Reader, error) {
	htmlFile := parseFlags()

	return os.Open(htmlFile)
}
