package models

type Link struct {
	HRef string `json:"href"`
	Text string `json:"text"`
}
