package usecase

import (
	"io"
	"strings"

	"gitlab.com/fredsar/htmlparser/internal/models"
	"golang.org/x/net/html"
)

func GetLinksFromHTML(reader io.Reader) ([]models.Link, error) {
	document, err := html.Parse(reader)
	if err != nil {
		return nil, err
	}
	aNodes := getANodes(document)
	links := make([]models.Link, 0, len(aNodes))
	for _, node := range aNodes {
		links = append(links, createLinkFromNode(node))
	}

	return links, nil
}

func createLinkFromNode(node *html.Node) models.Link {
	var link models.Link
	for _, attr := range node.Attr {
		if strings.EqualFold(attr.Key, "href") {
			link.HRef = attr.Val

			break
		}
	}

	link.Text = getTextFromANodes(node)

	return link
}

func getANodes(node *html.Node) []*html.Node {
	if node.Type == html.ElementNode && node.Data == "a" {
		return []*html.Node{node}
	}
	toReturn := make([]*html.Node, 0)
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		toReturn = append(toReturn, getANodes(c)...)
	}

	return toReturn
}

func getTextFromANodes(node *html.Node) string {
	if node.Type == html.TextNode {
		return node.Data
	}
	if node.Type != html.ElementNode {
		return ""
	}

	var toReturn string
	for c := node.FirstChild; c != nil; c = c.NextSibling {
		toReturn += getTextFromANodes(c) + " "
	}

	return strings.Join(strings.Fields(toReturn), " ")
}
